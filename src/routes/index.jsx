import { Route, Switch } from "react-router";
import Home from "../pages/Home";
import Signup from "../pages/Signup";
import Signin from "../pages/Signin";
import Dashboard from "../pages/Dashboard";
import { useEffect, useState } from "react";

const Routes = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    const token = JSON.parse(localStorage.getItem("@Doit:token"));

    if (token) {
      return setIsAuthenticated(true);
    }
  }, [isAuthenticated]);

  return (
    <Switch>
      <Route exact path="/">
        <Home isAuthenticated={isAuthenticated} />
      </Route>
      <Route path="/signup">
        <Signup isAuthenticated={isAuthenticated} />
      </Route>
      <Route path="/signin">
        <Signin
          isAuthenticated={isAuthenticated}
          setIsAuthenticated={setIsAuthenticated}
        />
      </Route>
      <Route path="/dashboard">
        <Dashboard isAuthenticated={isAuthenticated} />
      </Route>
    </Switch>
  );
};

export default Routes;
