import styled from "styled-components";

export const Container = styled.button`
  background: ${(props) =>
    props.whiteSchema ? "var(--white)" : "var(--black)"};
  color: ${(props) => (props.whiteSchema ? "var(--black)" : "var(--white)")};
  height: 45px;
  border-radius: 8px;
  border: 2px solid var(--black);
  font-family: "Roboto Mono", monospace;
  margin-top: 16px;
  width: 100%;
  transition: 0.2s;

  :hover {
    border: 2px solid var(--orange);
  }
`;
