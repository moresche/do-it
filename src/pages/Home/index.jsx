import { Redirect, useHistory } from "react-router";
import Button from "../../components/Button";
import { Container, Content } from "./styles";

const Home = ({ isAuthenticated }) => {
  const history = useHistory();

  const handleNavigation = (path) => {
    return history.push(path);
  };

  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Content>
        <h1>
          do<span>.</span>it
        </h1>
        <span>Organize-se de forma fácil & efetiva</span>
        <div>
          <Button onClick={() => handleNavigation("/signup")} whiteSchema>
            Cadastre-se
          </Button>
          <Button onClick={() => handleNavigation("/signin")}>Login</Button>
        </div>
      </Content>
    </Container>
  );
};

export default Home;
