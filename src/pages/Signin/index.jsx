import React from "react";
import { Background, Container, Content, AnimationContainer } from "./styles";
import Button from "../../components/Button/index";
import { Link, Redirect, useHistory } from "react-router-dom";
import Input from "../../components/Input";
import { FiMail, FiLock } from "react-icons/fi";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import { toast } from "react-toastify";

const Signin = ({ isAuthenticated, setIsAuthenticated }) => {
  const schema = yup.object().shape({
    email: yup.string().required("Digite seu email"),
    password: yup.string().required("Digite sua senha"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const history = useHistory();

  const onSubmitFunc = (data) => {
    api
      .post("/user/login", data)
      .then((response) => {
        const { token } = response.data;
        localStorage.setItem("@Doit:token", JSON.stringify(token));
        setIsAuthenticated(true);
        return history.push("/dashboard");
      })
      .catch((err) => toast.error("Email ou senha inválidos"));
  };

  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmitFunc)}>
            <h1>Login</h1>
            <Input
              register={register}
              name="email"
              error={errors.email?.message}
              icon={FiMail}
              label="Email"
              placeholder="Digite seu email"
            />
            <Input
              register={register}
              name="password"
              error={errors.password?.message}
              icon={FiLock}
              label="Senha"
              placeholder="Digite sua senha"
              type="password"
            />
            <Button type="submit">Entrar</Button>
            <p>
              Não possui uma conta? Faça seu <Link to="/signup">cadastro</Link>
            </p>
          </form>
        </AnimationContainer>
      </Content>
      <Background />
    </Container>
  );
};

export default Signin;
