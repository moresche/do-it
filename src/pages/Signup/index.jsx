import React from "react";
import { Background, Container, Content, AnimationContainer } from "./styles";
import Button from "../../components/Button/index";
import { Link, Redirect, useHistory } from "react-router-dom";
import Input from "../../components/Input";
import { FiUser, FiMail, FiLock } from "react-icons/fi";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../services/api";
import { toast } from "react-toastify";

const Signup = ({ isAuthenticated }) => {
  const schema = yup.object().shape({
    name: yup.string().required("Campo obrigatório."),
    email: yup.string().required("Campo obrigatório.").email("Email inválido"),
    password: yup
      .string()
      .required("Campo obrigatório.")
      .min(8, "Senha deve conter no mínimo 8 caracteres."),
    confirmPassword: yup
      .string()
      .required("Campo obrigatório.")
      .oneOf([yup.ref("password")], "Senhas não coincidem."),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const history = useHistory();

  const onSubmitFunc = ({ name, email, password }) => {
    const user = { name, email, password };
    api
      .post("user/register/", user)
      .then((_) => {
        toast.success("Conta criada com sucesso.");
        return history.push("/login");
      })
      .catch((err) =>
        toast.error("Erro ao criar a conta. Email já cadastrado.")
      );
  };

  if (isAuthenticated) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <Container>
      <Background />
      <Content>
        <AnimationContainer>
          <form onSubmit={handleSubmit(onSubmitFunc)}>
            <h1>Cadastro</h1>
            <Input
              register={register}
              name="name"
              error={errors.name?.message}
              icon={FiUser}
              label="Nome"
              placeholder="Digite seu nome completo"
            />
            <Input
              register={register}
              name="email"
              error={errors.email?.message}
              icon={FiMail}
              label="Email"
              placeholder="Digite seu melhor email"
            />
            <Input
              register={register}
              name="password"
              error={errors.password?.message}
              icon={FiLock}
              label="Senha"
              placeholder="Digite sua senha"
              type="password"
            />
            <Input
              register={register}
              name="confirmPassword"
              error={errors.confirmPassword?.message}
              icon={FiLock}
              label="Confirmação de Senha"
              placeholder="Confirme a senha digitada"
              type="password"
            />
            <Button type="submit">Enviar</Button>
            <p>
              Já possui uma conta? Faça um <Link to="/signin">login</Link>
            </p>
          </form>
        </AnimationContainer>
      </Content>
    </Container>
  );
};

export default Signup;
